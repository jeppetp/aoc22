(with [f (open "input")]
  (setv input_text (.readlines f)))

(setv input_text
      (lfor line input_text (.split (cut line -1))))

(print input_text)

(setv step 0)
(setv register_X 1)
(setv history_X [])
(setv i 0)
(for [line input_text]
  (print history_X)
  (print line)
  (cond
    (= (get line 0) "noop")
        (do
          (print "noop")
          (.append history_X register_X)
          (setv step (+ step 1)))
    (= (get line 0) "addx")
        (do
          (print "addx")
          (.append history_X register_X)
          (.append history_X register_X)
          (setv register_X (+ register_X (int (get line 1))))
          (setv step (+ step 2))))
)

(setv a_20 (* 20 (get history_X 19)))
(setv a_60 (* 60 (get history_X 59)))
(setv a_100 (* 100 (get history_X 99)))
(setv a_140 (* 140 (get history_X 139)))
(setv a_180 (* 180 (get history_X 179)))
(setv a_220 (* 220 (get history_X 219)))

(print a_20 a_60 a_100 a_140 (get history_X 180) a_180 (get history_X 219) a_220)
(print (+ a_20 a_60 a_100 a_140 a_180 a_220))
