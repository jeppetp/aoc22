(with [f (open "input")]
  (setv input_text (.readlines f)))

(setv input_text
      (lfor line input_text (.split (cut line -1))))


(setv step 0)
(setv register_X 1)
(setv history_X [])
(setv i 0)
(for [line input_text]
  (cond
    (= (get line 0) "noop")
        (do
          (.append history_X register_X)
          (setv step (+ step 1)))
    (= (get line 0) "addx")
        (do
          (.append history_X register_X)
          (.append history_X register_X)
          (setv register_X (+ register_X (int (get line 1))))
          (setv step (+ step 2))))
)

(defn print_line [offset]
  (for [x (range 0 40)]
    (if (<
          (abs (- (get history_X (+ offset x)) x))
          2)
      (print "#" :end "")
      (print "." :end "")
  ))
)

(for [x (range 6)] (print_line (* 40 x)) (print ""))
