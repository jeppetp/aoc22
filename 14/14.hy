(with [f (open "input")]
  (setv input_lines (.readlines f)))

(import time [sleep])

(setv input_lines_parsed (lfor line input_lines
                               (list (map eval (.split line " -> ")))))

(defn pair_to_line_points [pair]
  (cond
    (= (get pair 0 1) (get pair 1 1))
    (let [min_axis (min (get pair 0 0) (get pair 1 0))
          max_axis (max (get pair 0 0) (get pair 1 0))]
      (sfor x (range min_axis (+ 1 max_axis))
            #(x (get pair 0 1))))
    (= (get pair 0 0) (get pair 1 0))
    (let [min_axis (min (get pair 0 1) (get pair 1 1))
          max_axis (max (get pair 0 1) (get pair 1 1))]
      (sfor y (range min_axis (+ 1 max_axis))
            #((get pair 0 0) y)))
    ))

(setv max_x 0)
(setv min_x 10000)
(setv max_y 0)
(setv min_y 10000)
(for [line input_lines_parsed]
  (setv max_x (max max_x (max (lfor x line (get x 0))))))
(print max_x)
(for [line input_lines_parsed]
  (setv min_x (min min_x (min (lfor x line (get x 0))))))
(print min_x)
(for [line input_lines_parsed]
  (setv max_y (max max_y (max (lfor x line (get x 1))))))
(print max_y)
(for [line input_lines_parsed]
  (setv min_y (min min_y (min (lfor x line (get x 1))))))
(print min_y)

(setv walls (set))
(setv sand (set))
(for [line input_lines_parsed]
  (for [i (range (- (len line) 1))]
    (setv walls (| walls (pair_to_line_points (cut line i (+ i 2))))))
)

(defn print_grid [sand walls]
  (for [y (range min_y (+ 1 max_y))]
    (for [x (range min_x (+ 1 max_x))]
      (if (in #(x y) walls)
          (print "#" :end "")
          (if (in #(x y) sand)
              (print "o" :end "")
              (print "." :end "")
              )))
      (print "")
    )
)

(defn place_sand [pos sand walls]
;  (print_grid (| sand #{pos}) walls)
;  (print pos)
;  (print (in #((get pos 0) (+ 1 (get pos 1))) walls))
  (let [blocked_squares (| sand walls)
        x (get pos 0)
        y (get pos 1)]
    (cond
    (> y max_y) None
    (not (in #(x (+ y 1)) blocked_squares))
      (place_sand #(x (+ y 1)) sand walls)
    (not (in #((- x 1) (+ y 1)) blocked_squares))
    (place_sand #((- x 1) (+ y 1)) sand walls)
    (not (in #((+ x 1) (+ y 1)) blocked_squares))
    (place_sand #((+ x 1) (+ y 1)) sand walls)
    True  (| sand #{pos}))))

(setv i 0)
(while True
  (print "grain number" i)
  (setv sand (place_sand #(500 0) sand walls))
  (if sand
      (setv i (+ 1 i))
      (do
       (print i)
      (continue))
      )
)
