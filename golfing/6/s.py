g = (
    lambda x: list(
        map(len, map(set, zip(open("i").read()[i:] for i in range(x))))
    ).index(x)
    + x
)
print(g(4), g(14))
