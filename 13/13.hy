(import functools [cmp_to_key])
(defn first [l] (get l 0))
(defn rest [l] (cut l 1 None))

(defn parse_input [input_file [part_2 False]]
  (let
    [
     input_lines (with [f (open input_file)]
                   (.readlines f))
     left_input (cut input_lines 0 None 3)
     right_input (cut input_lines 1 None 3)
     all_input (list (+ left_input right_input))
     zipped_input (list (zip left_input right_input))
     parsed_input_list (lfor
                         ls all_input
                         (eval ls))
     parsed_input_pairs (lfor pair zipped_input
                              [(eval (get pair 0)) (eval (get pair 1))])
     ]
    (if part_2
        (+ parsed_input_list [[[2]] [[6]]])
        parsed_input_pairs)))

(defn order [l1 l2]
;  (print "  - Compare" l1 "vs" l2)
  (cond
    (and (isinstance l1 int) (isinstance l2 int))
    (cond
      (< l1 l2) (do
;                  (print "    - Left side is smaller, so inputs are in the right order")
                  1)
      (> l1 l2) (do
;                  (print "    - Right side is smaller, so inputs are no in the right order")
                  -1)
      (= l1 l2) 0)
    (and (isinstance l1 list) (isinstance l2 list))
    (cond (not (or l1 l2)) 0
          (not l1) 1
          (not l2) -1
          (and l1 l2) (if (order (first l1) (first l2))
                          (order (first l1) (first l2))
                          (order (rest l1) (rest l2)))
        )
    (and (isinstance l1 int) (isinstance l2 list))
    (order [l1] l2)
    (and (isinstance l1 list) (isinstance l2 int))
    (order l1 [l2])
))

(defn part_1 [input_file]
  (let [parsed_input (parse_input input_file :part_2 False)]
    (sum (lfor x (range (len parsed_input))
              :if (= 1 (order #*(get parsed_input x)))
              (+ x 1)))))

(defn part_2 [input_file]
  (let [parsed_input (parse_input input_file :part_2 True)
        sorted_list (sorted parsed_input :key (cmp_to_key order) :reverse True)]
    (* (+ 1 (.index sorted_list [[2]])) (+ 1 (.index sorted_list [[6]])))
  )
)

(print (part_1 "input"))
(print (part_2 "input"))
