(import time [sleep])
;(import rich [print])
(with [f (open "input")]
  (setv raw_input (.readlines f)))


(setv 2d-letters (list (map (fn [line] (list (map ord (list (cut line -1))))) raw_input)))

(setv x_max (- (len 2d-letters) 1))
(setv y_max (- (len (get 2d-letters 0)) 1))


(for [x (range (len 2d-letters))
      y (range (len (get 2d-letters 0)))]
     (cond (= 83 (get 2d-letters x y))
           (do
             ;PART 2: since all 'b's are in 2nd to leftmost column, i only need to check the rows that start with exactly 'abc'
             (setv starting-pos [27 0])
             (setv (get 2d-letters x y) (ord "a"))
             )

           (= 69 (get 2d-letters x y))
           (do
             (setv finish-pos [x y])
             (setv (get 2d-letters x y) (ord "z"))
             )
           True (print "" :end "")))

(print starting-pos finish-pos)

(defn neighbors [x y]
  (lfor x_rel [-1 0 1]
        y_rel [-1 0 1]
        :if (and (!= [x_rel y_rel] [0 0])
                 (<= (+ x x_rel) x_max)
                 (>= (+ x x_rel) 0)
                 (<= (+ y y_rel) y_max)
                 (>= (+ y y_rel) 0)
                 (not (and x_rel y_rel))
                 (>= (+ 1 (get 2d-letters x y)) (get 2d-letters (+ x x_rel) (+ y y_rel))))
        [(+ x x_rel)
         (+ y y_rel)]))

(print "test_neighbors" (neighbors 0 2))

(defn heuristic [p1]
  (+
    (abs (- (get p1 0) (get finish-pos 0)))
    (abs (- (get p1 1) (get finish-pos 1))))
  ;(- 122 (get 2d-letters (get p1 0) (get p1 1)))
)

(print "starting-post heuristic" (heuristic starting-pos))

(setv distances [[starting-pos 0]])
(setv visited [])

(defn print_visited [visited distances]
  (setv distance_squares (lfor x distances (get x 0)))
  (for [x (range (len 2d-letters))]
    (for [y (range (len (get 2d-letters 0)))]
      (if (in [x y] distance_squares)
          (print "O" :end "")
          (if (in [x y] visited)
              (print "#" :end "")
              (print "." :end ""))))
    (print "")
    ))



(while True
 ; (print "VISITED SQUARES: " (len visited))
;  (print_visited visited distances)
  (setv distance_squares (lfor x distances (get x 0)))
 ; (print distances)
 ; (print visited)
 ; (sleep .1)
  ; pop element and register as visited
  (setv current-pos (.pop distances))
  (setv visited (+ visited [(get current-pos 0)]))

  ;(print "current-pos" current-pos)
  (for [pos (neighbors #*(get current-pos 0))]
    ;(print "checking neighbor:" pos)
  ;  (print "current heuristic:" (heuristic pos))
    (if (and (not (in pos visited)) (not (in pos distance_squares)))
        (do
          (if (!= pos finish-pos)
            (do
    ;         (print "adding" [pos (+ 1 (get current-pos 1))])
              (setv distances (+ distances [[pos (+ 1 (get current-pos 1))]]))
            )
            (do
              (print "FOUND FINISH" [pos (+ 1 (get current-pos 1))])
              (print "DISTANCE IS:" (+ 1 (get current-pos 1)))
              (break)
              (setv distances (+ distances [[pos (+ 1 (get current-pos 1))]]))
            )

          )
      ;  (print distances)
        )
        (print "" :end ""))
  )
  ;(print "FIRST DISTANCE:" (get distances 0 ))
  (setv distances
        (sorted
          distances
          :key (fn [t]
                 (+ (heuristic (get t 0)) (get t 1)))
          :reverse True))
  ;(print distances)
)
