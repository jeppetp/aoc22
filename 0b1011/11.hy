(import rich [print])
(require hyrule [ap-each])


(defn monkey_parser [filename]
  ; Dictionary with monkey config, and list of items for each monkey
  (let [raw_input (with [f (open filename)]
                    (.read f))
        monkeys_input (.split raw_input "\n\n")]
    (zip #*(lfor monkey monkeys_input
      (let
          [lines (.split monkey "\n")
          monkey_number (get lines 0 7)
          monkey_items (list (map int (.split (cut (get lines 1) 18 None) ", ")))
          monkey_modulus (int (cut (get lines 3) 21 None))
          true_monkey (int (cut (get lines 4) 29 None))
          false_monkey (int (cut (get lines 5) 30 None))
          function_line (cut (get lines 2) 18 None)
          monkey_dict {"func" function_line
                        "mod" monkey_modulus
                        "if_true" true_monkey
                        "if_false" false_monkey}]
          [monkey_dict monkey_items]
        )
    ))
  )
)

;     function_line (cut (get lines 2) 18 None)
;     monkey_dict {"func" function_line
;                  "mod" monkey_modulus
;                  "if_true" true_monkey
;                  "if_false" false_monkey}]
;    (.append all_items monkey_items)
;    (.append monkeys monkey_dict)
;  )
;)
;(print monkeys)
;(print all_items)



(defn monkey_step [monkey_list item_list monkey_num part_2]
  (let [items (get item_list monkey_num)
        monkey_dict (get monkey_list monkey_num)
        inspected_items (list (map (fn [old] (eval (get monkey_dict "func"))) items))
        updated_worry_items (if part_2
                              (list (map (fn [val] (% val 9699690)) inspected_items))
                              (list (map (fn [val] (// val 3)) inspected_items)))
        true_list (lfor item updated_worry_items :if (= 0 (% item (get monkey_dict "mod") )) item)
        false_list (lfor item updated_worry_items :if (% item (get monkey_dict "mod") ) item)]
        (setv (get activity monkey_num) (+ (get activity monkey_num) (len inspected_items)))
          (lfor x (range (len monkey_list))
            (cond
              (= x monkey_num) []
              (= x (get monkey_dict "if_true")) (+ (get item_list x) true_list )
              (= x (get monkey_dict "if_false")) (+ (get item_list x) false_list )
              True (get item_list x)))
  )
)

(defn monkey_round [monkey_list item_list [part_2 False]]
  (setv return_val item_list)
  (ap-each (range (len monkey_list))
           (setv return_val (monkey_step monkey_list return_val it part_2)))
  return_val)

(defn sim_rounds [n monkey_list start_items [part_2 False]]
  (setv items start_items)
  (for [i (range n)]
    (setv items (monkey_round monkey_list items part_2)))
  (* #*(cut (sorted activity) -2 None)))

(setv activity (lfor x (range 8) 0))
(print (sim_rounds 20 #*(monkey_parser "input")))
(setv activity (lfor x (range 8) 0))
(print (sim_rounds 10000 #*(monkey_parser "input") True))
