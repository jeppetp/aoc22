(require hyrule [ap-each])

(defn line_to_vectors [line]
  (let [direction (get line 0)
        distance (int (get (.split (cut line -1)) 1))]
    (cond
      (= direction "U") (lfor x (range distance) #(0 1))
      (= direction "D") (lfor x (range distance) #(0 -1))
      (= direction "L") (lfor x (range distance) #(-1 0))
      (= direction "R") (lfor x (range distance) #(1 0)))))

(defn input_file_to_moves [filename]
  (with [f (open filename)]
    (lfor line (.readlines f)
          move (line_to_vectors line)
          move)))

(defn add_vectors [v1 v2]
  #((+ (get v1 0) (get v2 0)) (+ (get v1 1) (get v2 1)))
)

(defn sub_vectors [v1 v2]
  #((- (get v1 0) (get v2 0)) (- (get v1 1) (get v2 1)))
)

(defn sign [x]
  (cond
    (> x 0) 1
    (< x 0) -1
    (= x 0) 0))

(defn follow [head tail]
  (let
    [
     tail_x (get tail 0)
     tail_y (get tail 1)
     dist (sub_vectors head tail)
     dist_x (get dist 0)
     dist_y (get dist 1)
     normal_dist #((sign dist_x) (sign dist_y))
     ]
    (if (or (> (abs dist_x) 1) (> (abs dist_y) 1))
        (add_vectors tail normal_dist)
        tail
    )
  )
)

(defn move_rope [moves rope_chain]
  (do
    (setv visited_positions [#(0 0)])
    (ap-each moves
            (do
              (setv (get rope_chain 0) (add_vectors (get rope_chain 0) it))
              (for [i (range (- (len rope_chain) 1))]
                (setv (get rope_chain (+ 1 i)) (follow (get rope_chain i) (get rope_chain (+ 1 i)))))
              (.append visited_positions (get rope_chain -1))))
    (print (len (list (set visited_positions))))))

(setv rope_2 (lfor x (range 2) #(0 0)))
(setv rope_10 (lfor x (range 10) #(0 0)))

(move_rope (input_file_to_moves "input") (lfor x (range 2) #(0 0)))
(move_rope (input_file_to_moves "input") (lfor x (range 10) #(0 0)))
